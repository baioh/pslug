const regRule = /(\s|\/)/g

module.exports = function(input) {
  return input
    .trim()
    .replace(regRule, '-')
    .toLowerCase()
}
